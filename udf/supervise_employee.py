#!/usr/bin/env python
from deepdive import *
import random
from collections import namedtuple
import sys
EmployeeLabel = namedtuple('EmployeeLabel', 'p_id, o_id, label, type')

@tsv_extractor
@returns(lambda
        p_id   = "text",
        o_id   = "text",
        label   = "int",
        rule_id = "text",
    :[])
# heuristic rules for finding positive/negative examples of spouse relationship mentions
def supervise(
        p_id="text", p_begin="int", p_end="int",
        o_id="text", o_begin="int", o_end="int",
        doc_id="text", sentence_index="int", sentence_text="text",
        tokens="text[]", lemmas="text[]", pos_tags="text[]", ner_tags="text[]",
        dep_types="text[]", dep_token_indexes="int[]",
    ):

    # Constants
    EMPLOYEE = frozenset(["employee", "cto", "cpo", "founder", "president",
                          "works for", "employed", "ceo", "engineer"])
    MAX_DIST = 10

    # Common data objects
    first_end_idx = min(p_end, o_end)
    second_start_idx = max(p_begin, o_begin)
    second_end_idx = max(p_end,o_end)
    intermediate_lemmas = lemmas[first_end_idx+1:second_start_idx]
    intermediate_ner_tags = ner_tags[first_end_idx+1:second_start_idx]
    tail_lemmas = lemmas[second_end_idx+1:]
    employee = EmployeeLabel(p_id=p_id, o_id=o_id, label=None, type=None)

    # Rule: Candidates that are too far apart
    if len(intermediate_lemmas) > MAX_DIST:
        yield employee._replace(label=-1, type='neg:far_apart')

    # Rule: Candidates that have a third person in between
    if 'PERSON' in intermediate_ner_tags:
        yield employee._replace(label=-1, type='neg:third_person_between')

    # Rule: Sentences that contain wife/husband in between
    #         (<p>)([ A-Za-z]+)(wife|husband)([ A-Za-z]+)(<o>)
    if len(EMPLOYEE.intersection(intermediate_lemmas)) > 0:
        yield employee._replace(label=1, type='pos:keyword_in_between')
