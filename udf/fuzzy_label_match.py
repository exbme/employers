#!/usr/bin/env python
from deepdive import *  # Required for @tsv_extractor and @returns
from fuzzywuzzy import fuzz

@tsv_extractor  # Declares the generator below as the main function to call
@returns(lambda # Declares the types of output columns as declared in DDlog
         p_id = "text",
         o_id = "text",
         label = "int",
         rule_id = "text",
     :[])
def classify(   # The input types can be declared directly on each parameter as its default value
        p_id = "text",
        p_name = "text",
        o_id = "text",
        o_name = "text",
        pdb_name = "text",
        odb_name = "text",
        label = "int",
        rule_id = "text"
    ):
    """
    Classify articles by assigning topics.
    """
    person_ratio = fuzz.ratio(p_name.lower(), pdb_name.lower())
    org_ratio = fuzz.ratio(o_name.lower(), odb_name.lower())
    if person_ratio > 90 and org_ratio > 90:
        yield [p_id, o_id, label, rule_id]

