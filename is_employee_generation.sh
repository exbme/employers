#!/bin/sh

deepdive sql eval "
SELECT hsi.p_id
     , hsi.o_id
     , s.doc_id
     , s.sentence_index
     , hsi.label
     , hsi.expectation
     , s.tokens
     , pm.mention_text AS p_text
     , pm.begin_index  AS p_start
     , pm.end_index    AS p_end
     , om.mention_text AS o_text
     , om.begin_index  AS o_start
     , om.end_index    AS o_end

  FROM is_employee_label_inference hsi
     , person_mention             pm
     , organization_mention       om
     , sentences                  s

 WHERE hsi.p_id          = pm.mention_id
   AND pm.doc_id         = s.doc_id
   AND pm.sentence_index = s.sentence_index
   AND hsi.o_id          = om.mention_id
   AND om.doc_id         = s.doc_id
   AND om.sentence_index = s.sentence_index
   AND      expectation >= 0.5

 ORDER BY random()
 LIMIT 100

" format=csv header=1 >labeling/is_employee-precision/is_employee.csv